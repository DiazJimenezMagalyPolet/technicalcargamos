export class Task{
  uid?: string;
  key?: string;
  description?: string;
  finished?: boolean;
  createAt?: string;
  duration? : number;
  hours ? : number;
  minutes ? : number;
  timeComplete?:number;
  left?:number;
}
