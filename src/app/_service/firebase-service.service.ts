import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireObject} from '@angular/fire/database';

@Injectable({
  providedIn: 'root'
})
export class FirebaseServiceService {
  tutorial: AngularFireObject<any>;

  constructor(private db: AngularFireDatabase) { }
}
