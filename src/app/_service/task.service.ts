import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';
import { Router } from '@angular/router';
import { Task } from '../_models/task';
import { AuthService } from '../_services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class TaskService {
  private dbPath = '/tasks';

  tasksRef: AngularFireList<Task>;

  constructor(
    private authService: AuthService,
    private db: AngularFireDatabase,
    public router: Router) {
      if(this.authService.isLoggedIn){
        var myUserId = this.authService.userData.uid;
        this.tasksRef = db.list(this.dbPath+'/'+myUserId);      
      }
      else{
        this.authService.SignOut();
      }
  }
  
  getAll(): AngularFireList<Task> {
    return this.tasksRef;
  }

  create(task: Task): any {
    return this.tasksRef.push(task);
  }

  update(key: string, value: any): Promise<void> {
    return this.tasksRef.update(key, value);
  }

  delete(key: string): Promise<void> {
    return this.tasksRef.remove(key);
  }

  deleteAll(): Promise<void> {
    return this.tasksRef.remove();
  }
}
