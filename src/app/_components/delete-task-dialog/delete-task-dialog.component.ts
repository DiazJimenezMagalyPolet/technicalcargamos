import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { TaskService } from '../../_service/task.service';
import { Task } from '../../_models/task';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-delete-task-dialog',
  templateUrl: './delete-task-dialog.component.html',
  styleUrls: ['./delete-task-dialog.component.css']
})
export class DeleteTaskDialogComponent implements OnInit {

  constructor(
    private taskService: TaskService,
    public dialogRef: MatDialogRef<DeleteTaskDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public task: Task,
    private _snackBar: MatSnackBar) { }

  ngOnInit(): void {
  }


  removeTask(): void {
    this.taskService.delete(this.task.key).then(() => {
      this._snackBar.open('Tarea eliminada', 'Cerrar', {
        duration: 5000,
        horizontalPosition: "left",
        panelClass: ["snackbar-correct"]
      });
    });
  }
}
