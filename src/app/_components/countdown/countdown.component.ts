import { Component, Input, ViewChild, Output, EventEmitter, OnChanges, SimpleChanges, AfterViewInit, OnDestroy } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Task } from 'src/app/_models/task';
import { TaskService } from 'src/app/_service/task.service';

@Component({
  selector: 'app-countdown',
  templateUrl: './countdown.component.html',
  styleUrls: ['./countdown.component.css']
})
export class CountdownComponent implements OnChanges, OnDestroy {
  @ViewChild('cd', { static: true }) countdown: CountdownComponent;
  @Input()
  task: Task;
  @Input()
  updateVal: boolean;
  left: number; 
  @Output () valueLeft: EventEmitter<number> = new EventEmitter();

  constructor(
    private taskService: TaskService,
    private _snackBar: MatSnackBar) { 
  }
  ngOnChanges(changes: SimpleChanges): void {
    if(this.updateVal){
      let newLeft = this.countdown.left/1000; 
      console.log("ngOnChanges",newLeft)
    }
  }

  ngOnDestroy(): void {  }

  stopEvent(){
    this.countdown.stopEvent();
    let timeComplete = this.task.duration - this.countdown.left;
    this.taskService.update(this.task.key, {timeComplete: timeComplete}).then(() => {
      this._snackBar.open('Tarea finalizada', 'Cerrar', {
        duration: 5000,
        horizontalPosition: "left",
        panelClass: ["snackbar-correct"]
      });
    });
  }

}
