import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { Task } from '../../_models/task';
import { TaskService } from '../../_service/task.service';
import { map } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material/snack-bar';
import { CreateTaskDialogComponent } from '../create-task-dialog/create-task-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { DeleteTaskDialogComponent } from '../delete-task-dialog/delete-task-dialog.component';

@Component({
  selector: 'app-table-tasks',
  templateUrl: './table-tasks.component.html',
  styleUrls: ['./table-tasks.component.css']
})
export class TableTasksComponent implements AfterViewInit, OnInit {
  displayedColumns: string[] = ['description','duration','control','complete','timeComplete','opciones'];
  dataSource = new MatTableDataSource();
  tasks?: Task[];
  currentTask?: Task;
  currentIndex = -1;
  title = '';
  updateVal : boolean = false;

  @ViewChild(MatSort) sort: MatSort;
  
  constructor(
    private taskService: TaskService,
    private _snackBar: MatSnackBar,
    private dialog: MatDialog) { }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
  }

  ngOnInit(): void {
    this.retrieveTasks();
  }
  
  openEditTaskDialog(task): void {
    this.currentTask = task;
    const dialogRef = this.dialog.open(CreateTaskDialogComponent, {
      data: {
        key : this.currentTask.key,
        description: this.currentTask.description,
        duration: this.currentTask.duration
      }
    });
  }
  refreshList(): void {
    this.currentTask = undefined;
    this.currentIndex = -1;
    this.retrieveTasks();
  }

  retrieveTasks(): void {
    this.taskService.getAll().snapshotChanges().pipe(
      map(changes =>
        changes.map(c =>
          ({ key: c.payload.key, ...c.payload.val() })
        )
      )
    ).subscribe(data => {
      this.tasks = data;
      this.dataSource = new MatTableDataSource(this.tasks);
    });
  }

  setActiveTask(task: Task, index: number): void {
    this.currentTask = task;
    this.currentIndex = index;
  }

  openRemoveTaskDialog(element): void {    
    const dialogRef = this.dialog.open(DeleteTaskDialogComponent, {
      data: {
        key : element.key
      }
    });
  }
  toggleChange(element): void {
    this.updateVal = true;
    if(!element.finished) element.finished = true;
    else element.finished = false;
    
    this.taskService.update(element.key, {finished: element.finished}).then(() => {
      this.updateVal = false;
      if(element.finished){
        this._snackBar.open('Tarea finalizada', 'Cerrar', {
          duration: 5000,
          horizontalPosition: "left",
          panelClass: ["snackbar-correct"]
        });
      }else{
        this._snackBar.open('Tarea abierta', 'Cerrar', {
          duration: 5000,
          horizontalPosition: "left",
          panelClass: ["snackbar-correct"]
        });
      }
    });
  }

  removeAllTasks(): void {
    this.taskService.deleteAll()
      .then(() => this.refreshList())
      .catch(err => console.log(err));
  }
  
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  getDuration(element) {
    var tiempoCumplido = element.duration - element.left;
    var hours = Math.floor( tiempoCumplido / 3600 );  
    var minutes = Math.floor( (tiempoCumplido % 3600) / 60 );
    var seconds = tiempoCumplido % 60;
    var minStr = "";
    var segStr = "";
    //Anteponiendo un 0 a los minutos si son menos de 10 
    minStr = minutes < 10 ? '0' + minutes : minutes.toString();

    //Anteponiendo un 0 a los segundos si son menos de 10 
    segStr = seconds < 10 ? '0' + seconds : seconds.toString();

    var result = hours + ":" + minStr + ":" + segStr;  // 2:41:30
    return result;
  }

  secondsToString(element) {
    var hours = Math.floor( element / 3600 );  
    var minutes = Math.floor( (element % 3600) / 60 );
    var seconds = element % 60;
    var minStr = "";
    var segStr = "";
    //Anteponiendo un 0 a los minutos si son menos de 10 
    minStr = minutes < 10 ? '0' + minutes : minutes.toString();

    //Anteponiendo un 0 a los segundos si son menos de 10 
    segStr = seconds < 10 ? '0' + seconds : seconds.toString();

    var result = hours + ":" + minStr + ":" + segStr;  // 2:41:30
    return result;
  }
} 
