import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { TaskService } from '../../_service/task.service';
import { Task } from '../../_models/task';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AuthService } from 'src/app/_services/auth.service';

@Component({
  selector: 'app-create-task-dialog',
  templateUrl: './create-task-dialog.component.html',
  styleUrls: ['./create-task-dialog.component.css']
})

export class CreateTaskDialogComponent{
  sizeTask: string = "1"; 
  constructor(
    private taskService: TaskService,
    private authService: AuthService,
    public dialogRef: MatDialogRef<CreateTaskDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public task: Task,
    private _snackBar: MatSnackBar) { 
      switch (this.task.duration) {
        case 1800:
          this.sizeTask = "1";
          break;
        case 2700:
          this.sizeTask = "2";        
          break;
        case 3600:
          this.sizeTask = "3";
          break;
        default:
          this.sizeTask = "4";
          break;
      }    
     }

  saveTask(): void {
    this.task.uid = this.authService.userData.uid;
    switch (this.sizeTask) {
      case "1":
        this.task.duration = 1800;
        break;
      case "2":
        this.task.duration = 2700;        
        break;
      case "3":
        this.task.duration = 3600;
        break;
      case "4":
        this.task.duration = (this.task.hours*3600) + (this.task.minutes * 60);
        break;
    }    
    this.task.left = this.task.duration;
    if(this.task.key){
      this.taskService.update(this.task.key, {description: this.task.description, duration: this.task.duration}).then(() => {
        this._snackBar.open('Tarea actualizada correctamente', 'Cerrar', {
          duration: 5000,
          horizontalPosition: "left",
          panelClass: ["snackbar-correct"]
        });
        this.dialogRef.close();
      });
    }else{
      this.taskService.create(this.task).then(() => {
        this._snackBar.open('Tarea creada correctamente', 'Cerrar', {
          duration: 5000,
          horizontalPosition: "left",
          panelClass: ["snackbar-correct"]
        });
        this.dialogRef.close();
      });
    }
  }
}
