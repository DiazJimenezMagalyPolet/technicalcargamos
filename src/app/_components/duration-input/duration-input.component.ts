import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-duration-input',
  templateUrl: './duration-input.component.html',
  styleUrls: ['./duration-input.component.css']
})
export class DurationInputComponent implements OnInit {
  @Input() durationString;
  @Input() disabled;
  @Output() durationStringChange: EventEmitter<string> = new EventEmitter<string>();

  hours;
  minutes;
  seconds;

  constructor() { }

  ngOnInit(): void {    
    if (this.durationString) {
      let splitDuration = this.durationString.split(':');

      this.hours = splitDuration[0];
      this.minutes = splitDuration[1];
      this.seconds = splitDuration[2];
    }
  }
  check(type) {
    if (type === 'hours') {
      this.hours = this.getValidValue(this.hours, 23);
    } else if (type === 'minutes') {
      this.minutes = this.getValidValue(this.minutes, 59);
    } else if (type === 'seconds') {
      this.seconds = this.getValidValue(this.seconds, 59);
    }

    this.durationString = `${this.hours}:${this.minutes}:${this.seconds}`;
    this.durationStringChange.emit(this.durationString);
  }

  getValidValue(value, max) {
    let n;
    if (/^\d+$/.test(value)) {
      n = parseInt(value);
      n = Math.max(0, n);
      n = Math.min(max, n);
      return new String(n);
    } else {
      return new String('0');
    }
  }
}
