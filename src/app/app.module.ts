import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';

import { AngularFireAuthModule } from "@angular/fire/auth";
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { environment } from '../environments/environment';
import { AuthService } from "./_services/auth.service";

import { CountdownModule } from 'ngx-countdown';
import { MatDialogModule} from '@angular/material/dialog';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatButtonModule } from '@angular/material/button';
import { MatTableModule } from '@angular/material/table';
import { MatIconModule } from '@angular/material/icon';
import { LoginComponent } from './_components/auth/login/login.component';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatRadioModule} from '@angular/material/radio';
import {MatCardModule} from '@angular/material/card';

import { AppRoutingModule } from './app-routing.module';
import { SigninComponent } from './_components/signin/signin.component';
import { DurationInputComponent } from './_components/duration-input/duration-input.component';
import { LayoutComponent } from './_components/layout/layout.component';
import { NavbarComponent } from './_components/layout/navbar/navbar.component';
import { DashboardComponent } from './_components/dashboard/dashboard.component';
import { CreateTaskDialogComponent } from './_components/create-task-dialog/create-task-dialog.component';
import { TableTasksComponent } from './_components/table-tasks/table-tasks.component';
import { DeleteTaskDialogComponent } from './_components/delete-task-dialog/delete-task-dialog.component';
import { CountdownComponent } from './_components/countdown/countdown.component';

@NgModule({
  declarations: [
    AppComponent,
    LayoutComponent,
    NavbarComponent,
    DashboardComponent,
    CreateTaskDialogComponent,
    TableTasksComponent,
    LoginComponent,
    DeleteTaskDialogComponent,
    CountdownComponent,
    SigninComponent,
    DurationInputComponent,
  ],
  imports: [
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    AngularFirestoreModule,
    BrowserModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatFormFieldModule,
    FormsModule,
    ReactiveFormsModule,
    MatSidenavModule,
    MatButtonModule,
    MatDialogModule,
    MatTableModule,
    MatIconModule,
    MatSnackBarModule,
    MatSlideToggleModule,
    MatGridListModule,
    CountdownModule,
    MatRadioModule,
    AppRoutingModule,
    MatCardModule
  ],
  providers: [AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }
